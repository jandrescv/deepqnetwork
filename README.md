# DQN - Deep Reinforcement Learning

This package tries to replicate the results obtained by DeepMind in the following papers:

* [Human-level control through deep reinforcement learning - Nature - 2015](http://www.nature.com/nature/journal/v518/n7540/full/nature14236.html)
* [Playing Atari with Deep Reinforcement Learning - NIPS - 2013](https://www.cs.toronto.edu/~vmnih/docs/dqn.pdf)

#### Breakout:

![breakout_gif](media/breakout.gif)

#### Seaquest:

![seaquest_gif](media/seaquest.gif)

## Dependencies

* Nvidia GPU with compute capability >= 3.0
* CUDA >= 7.0
* Python 2.7.6
* Theano
* Arcade Learning Environment
* Python OpenCV
* NumPy

#### Optional

* cuDNN >= 3.0
* SciPy
* Pillow
* Matplotlib

## Easy Installation:

    git clone https://github.com/andrescv/DeepQNetwork.git
    cd DeepQNetwork/dependencies
    chmod a+x install_dependencies.sh
    ./install_dependencies.sh

## Train:

    python train.py -rom_name breakout

## Test:

    python test.py -rom_name breakout -net_file breakout_weights_epoch_25.pkl.gz -episodes 10

add **-make_gif true** to generate a gif or/and **-make_video true** to generate a video

## Videos:

#### Breakout:

[![Breakout](http://img.youtube.com/vi/97M0nW4OeLA/0.jpg)](https://youtu.be/97M0nW4OeLA)

#### Seaquest:

[![Seaquest](http://img.youtube.com/vi/q736UP-Nb3c/0.jpg)](https://youtu.be/q736UP-Nb3c)
