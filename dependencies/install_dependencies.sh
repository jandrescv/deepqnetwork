sudo apt-get update

sudo apt-get install git python-pip python-dev libfreetype6-dev libpng-dev python-opencv python-nose libatlas-dev libopenblas-dev liblapack-dev libav-tools gfortran g++

sudo pip install --upgrade numpy
sudo pip install --upgrade scipy
sudo pip install --upgrade pillow
sudo pip install --upgrade matplotlib
sudo pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git

sudo apt-get install libsdl1.2-dev libsdl-gfx1.2-dev libsdl-image1.2-dev cmake
git clone https://github.com/mgbellemare/Arcade-Learning-Environment ALE
cd ./ALE
cmake -DUSE_SDL=ON -DUSE_RLGLUE=OFF .
make -j8
sudo pip install .
cd ..
sudo rm -r ALE
