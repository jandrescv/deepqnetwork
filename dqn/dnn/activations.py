#!/usr/bin/env python
# coding=utf-8

import theano.tensor as T

def relu(x):

    return T.nnet.relu(x)

def linear(x):

    return x
