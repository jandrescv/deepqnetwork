#!/usr/bin/env python
# coding=utf-8

import theano
import numpy as np
import theano.tensor as T
import theano.sandbox.cuda.dnn as dnn

from random import get_rng
from theano import shared

gpu = False
cuDNN = False
floatX = theano.config.floatX

# Verificar que el device sea gpu
if(theano.config.device[0:3] == 'gpu'):
    print "GPU Backend"
    gpu = True

# Si esta instalado cuDNN (V3 o mayor)
if(gpu and dnn.dnn_available()):
    print "cuDNN Available"
    cuDNN = True

"""
    En el paper de DeepMind no dicen como inicializaron los pesos, sin embargo
    en la documentacion de torch usan algo parecido a HeUniform, lo raro es que
    lo usan en el bias tambien (En la clase de standford recomiendan que el bias sea 0).

    La inicializacion entonces es:

    scale = 1.0 / np.sqrt(fan_in)
    initW = rng.uniform(low=-scale, high=scale, size=shape)
    initb = rng.uniform(low=-scale, high=scale, size=shape)
"""

class SpatialConvolution:

    def __init__(self, input, filter_shape, input_shape, activation, subsample=(1, 1)):

        self.input = input
        self.activation = activation
        self.subsample = subsample
        self.input_shape = input_shape
        self.filter_shape = filter_shape
        scale = 1.0 / np.sqrt(np.prod(input_shape[1:]))
        initW = np.asarray(get_rng().uniform(low=-scale, high=scale, size=filter_shape), dtype=floatX)
        initb = np.asarray(get_rng().uniform(low=-scale, high=scale, size=(filter_shape[0],)), dtype=floatX)
        self.W = shared(initW)
        self.b = shared(initb)

    def getOutput(self):
        if(gpu and cuDNN):
            self.conv = dnn.dnn_conv(
                self.input,
                self.W,
                subsample=self.subsample
            )
        else:
            self.conv = T.nnet.conv.conv2d(
                self.input,
                self.W,
                subsample=self.subsample
            )

        return self.activation(self.conv + self.b.dimshuffle('x',0,'x','x'))

    def getOutputDim(self):

        k = self.filter_shape[0]
        h = (self.input_shape[1]-self.filter_shape[2])/self.subsample[0] + 1
        w = (self.input_shape[2]-self.filter_shape[3])/self.subsample[1] + 1

        return (k, h, w)

    def getParams(self):

        return [self.W, self.b]

class Flatten:

    def __init__(self, input, input_shape, outdims=2):

        assert(outdims > 1 and outdims <= len(input_shape))

        self.input = input
        self.input_shape = input_shape
        self.outdims = outdims

    def getOutput(self):

        return self.input.flatten(self.outdims)

    def getOutputDim(self):

        return np.prod(self.input_shape[(self.outdims - 2):])

class Dense:

    def __init__(self, input, input_units, output_units, activation):

        self.input = input
        self.activation = activation
        self.output_units = output_units
        scale = 1.0 / np.sqrt(input_units)
        initW = np.asarray(get_rng().uniform(low=-scale, high=scale, size=(input_units, output_units)), dtype=floatX)
        initb = np.asarray(get_rng().uniform(low=-scale, high=scale, size=(output_units,)), dtype=floatX)
        self.W = shared(initW)
        self.b = shared(initb)

    def getOutput(self):

        return self.activation(T.dot(self.input, self.W) + self.b)

    def getOutputDim(self):

        return self.output_units

    def getParams(self):

        return [self.W, self.b]
