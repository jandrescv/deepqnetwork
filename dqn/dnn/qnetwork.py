#!/usr/bin/env python
# coding=utf-8

import theano.tensor as T

from activations import relu
from activations import linear
from layers import SpatialConvolution
from layers import Flatten
from layers import Dense

class DeepQNetwork:

    def __init__(self, n_actions, histLen, width, height):

        self.input = T.tensor4(name='input')
        preprocess = self.input / 255.0
        self.params = [ ]

        input_shape = (histLen, height, width)

        self.conv1 = SpatialConvolution(
            preprocess,
            (32, 4, 8, 8),
            input_shape,
            relu,
            subsample=(4, 4)
        )
        self.params = self.params + self.conv1.getParams()

        self.conv2 = SpatialConvolution(
            self.conv1.getOutput(),
            (64, 32, 4, 4),
            self.conv1.getOutputDim(),
            relu,
            subsample=(2, 2)
        )
        self.params = self.params + self.conv2.getParams()

        self.conv3 = SpatialConvolution(
            self.conv2.getOutput(),
            (64, 64, 3, 3),
            self.conv2.getOutputDim(),
            relu,
            subsample=(1, 1)
        )
        self.params = self.params + self.conv3.getParams()

        self.flatten1 = Flatten(
            self.conv3.getOutput(),
            self.conv3.getOutputDim(),
            outdims=2
        )

        self.dense1 = Dense(
            self.flatten1.getOutput(),
            self.flatten1.getOutputDim(),
            512,
            relu
        )
        self.params = self.params + self.dense1.getParams()

        self.dense2 = Dense(
            self.dense1.getOutput(),
            self.dense1.getOutputDim(),
            n_actions,
            linear
        )
        self.params = self.params + self.dense2.getParams()

        self.qvalues = self.dense2.getOutput()

    def getWeights(self):

        weights = [p.get_value(borrow=True) for p in self.params]
        return weights

    def setWeights(self, weights):

        for p, v in zip(self.params, weights):
            p.set_value(v, borrow=False)
