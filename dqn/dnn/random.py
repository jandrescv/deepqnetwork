#!/usr/bin/env python
# coding=utf-8

import numpy as np

_rng = np.random.RandomState()

def get_rng():

    return _rng

def set_rng(rng):

    global _rng
    _rng = rng
