#!/usr/bin/env python
# coding=utf-8

import random
import numpy as np

class ReplayMemory:

    def __init__(self, settings):

        self.width = settings['RESIZE_WIDTH']
        self.height = settings['RESIZE_HEIGHT']
        self.size = settings['REPLAY_MEMORY_SIZE']
        self.histLen = settings['AGENT_HISTORY_LENGTH']
        self.batch_size = settings['BATCH_SIZE']

        self.s = np.zeros((self.size, self.height, self.width), dtype='uint8')
        self.a = np.zeros((self.size,), dtype='uint8')
        self.r = np.zeros((self.size,), dtype='int32')
        self.t = np.zeros((self.size,), dtype='uint8')

        self.recent_buf_s = [ ]
        self.recent_buf_t = [ ]
        self.recent_s = np.zeros((self.histLen, self.height, self.width), dtype='uint8')

        self.buf_s1 = np.zeros((self.batch_size, self.histLen, self.height, self.width), dtype='uint8')
        self.buf_a = np.zeros((self.batch_size,), dtype='uint8')
        self.buf_r = np.zeros((self.batch_size,), dtype='int32')
        self.buf_t = np.zeros((self.batch_size,), dtype='uint8')
        self.buf_s2 = np.zeros((self.batch_size, self.histLen, self.height, self.width), dtype='uint8')

        self.num_entries = 0
        self.insert_index = 0

    def currentSize(self):

        return self.num_entries

    def isEmpty(self):

        return (self.num_entries == 0)

    def reset(self):

        self.insert_index = 0
        self.num_entries = 0

    def storeTransition(self, screen, action, reward, terminal):

        if(self.num_entries < self.size):
            self.num_entries += 1

        if(self.insert_index == self.size):
            self.insert_index = 0

        self.s[self.insert_index] = screen
        self.a[self.insert_index] = action
        self.r[self.insert_index] = reward
        self.t[self.insert_index] = terminal
        self.insert_index += 1

    def storeRecentState(self, screen, term):

        if(len(self.recent_buf_s) == 0):
            for i in xrange(self.histLen-1):
                self.recent_buf_s.append(np.zeros_like(screen))
                self.recent_buf_t.append(1)

        self.recent_buf_s.append(screen)
        self.recent_buf_t.append(1 if (term) else 0)

        if(len(self.recent_buf_s) > self.histLen):
            del(self.recent_buf_s[0])
            del(self.recent_buf_t[0])

    def getRecentState(self):

        self.recent_s[:,:,:] = self.recent_buf_s
        if(np.any(self.recent_buf_t[0:self.histLen-1])):
            zero_out_index = np.max(np.nonzero(self.recent_buf_t[0:self.histLen-1])) + 1
            self.recent_s[0:zero_out_index] = 0

        return self.recent_s

    def sampleMinibatch(self):

        count = 0
        while(count < self.batch_size):

            index = random.randint(0, self.num_entries - self.histLen - 1)

            if(np.any(self.t[index:index+self.histLen])):
                continue

            self.buf_s1[count] = self.s[index:index+self.histLen]
            self.buf_a[count] = self.a[index+self.histLen-1]
            self.buf_r[count] = self.r[index+self.histLen-1]
            self.buf_t[count] = self.t[index+self.histLen]
            self.buf_s2[count] = self.s[index+1:index+1+self.histLen]
            count += 1

        return self.buf_s1, self.buf_a, self.buf_r, self.buf_t, self.buf_s2
