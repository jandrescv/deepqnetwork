#!/usr/bin/env python
# coding=utf-8

import os
import gzip
import cPickle
import argparse
import numpy as np
import matplotlib.pyplot as plt

def main(arg):

    rh_dir = os.path.join('results', arg.name+"_reward_history.pkl.gz")
    eh_dir = os.path.join('results', arg.name+"_episode_history.pkl.gz")

    f = gzip.open(rh_dir, 'rb')
    reward_history = cPickle.load(f)
    reward_history = [0] + reward_history
    f.close()

    f = gzip.open(eh_dir, 'rb')
    episode_history = cPickle.load(f)
    episode_history = [0] + episode_history
    f.close()

    epochs = range(0, len(reward_history))

    plt.subplot(1,2,1)
    plt.title('Reward Results')
    plt.plot(epochs, reward_history, 'r-', linewidth=1.5)
    plt.grid(True)
    plt.axis([0, len(reward_history) - 1, 0, np.max(reward_history) + 10])
    plt.ylabel('Average Reward Per Episode')
    plt.xlabel('Epoch')
    plt.subplot(1,2,2)
    plt.title('Episode Results')
    plt.plot(epochs, episode_history, 'b-', linewidth=1.5)
    plt.axis([0, len(episode_history) - 1, 0, np.max(episode_history)+ 10])
    plt.grid(True)
    plt.ylabel('No. Episodes Per Test')
    plt.xlabel('Epoch')
    plt.show()

if __name__ == '__main__':

    parser = argparse.ArgumentParser('Plot Results')
    parser.add_argument('-name', '--name', help='Name (e.g. breakout)')
    arg = parser.parse_args()
    main(arg)
