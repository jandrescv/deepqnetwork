#!/usr/bin/env python
# coding=utf-8

import os
import gzip
import cPickle
import argparse
import numpy as np
import scipy.misc as misc

from utils import writeGif
from dqn.dnn import set_rng
from hyperparameters import Hyperparameters
from dqn.environment import GameEnvironment
from dqn.agent import NeuralQLearner

def main(environment_settings, agent_settings, args):

    game_env = GameEnvironment(environment_settings)
    rom_name = game_env.name
    agent_settings['N_ACTIONS'] = game_env.n_actions
    agent = NeuralQLearner(agent_settings)
    frame_skip = environment_settings['FRAME_SKIP']
    f = gzip.open(os.path.join('snapshots', args.net_file), 'rb')
    w = cPickle.load(f)
    f.close()
    agent.net.setWeights(w)

    makeGif = args.make_gif
    makeVideo = args.make_video
    bd = True
    count = 1
    frames = []
    screen, reward, terminal, game_over = game_env.newTestGame()

    frames.append(game_env.getScreenRGB())

    if(makeVideo):
        misc.imsave('media/'+str(count)+'.png', frames[count-1])
        count += 1

    for i in xrange(args.episodes):

        while True:

            action = agent.perceive(screen, reward, terminal, True)
            screen, reward, terminal, game_over = game_env.step(action, training=False)

            if(bd):
                frames.append(game_env.getScreenRGB())

            if(makeVideo and bd):
                misc.imsave('media/'+str(count)+'.png', frames[count-1])
                count += 1

            if(terminal):
                screen, reward, terminal, game_over = game_env.newGame()

                if(bd):
                    frames.append(game_env.getScreenRGB())

                if(makeVideo and bd):
                    misc.imsave('media/'+str(count)+'.png', frames[count-1])
                    count += 1

                bd = False
                break

    if(makeGif):
        writeGif('media/'+rom_name+'.gif', frames, duration=float(1)/float(30), repeat=True)

    if(makeVideo):
        os.system('avconv -r '+str(int(60/max(1, frame_skip)))+' -i media/%d.png -f mov -c:v libx264 -y media/'+rom_name+'.mov')
        os.system('rm media/*.png')

if __name__ == '__main__':

    def strToBool(str):
        if(str.upper() in ['T','TRUE','1']):
            return True
        else:
            return False

    p = Hyperparameters()
    parser = argparse.ArgumentParser('Training Agent')
    parser.add_argument('-episodes', '--episodes', type=int, default=1, help='Steps x Test')
    parser.add_argument('-net_file', '--net_file', type=str, help='Net File (e.g. breakout_weighs_epoch_20.pkl.gz)', required=True)
    parser.add_argument('-roms_dir', '--roms_dir', type=str, default=p.ROMS_DIR, help='ROMS Directory')
    parser.add_argument('-rom_name', '--rom_name', type=str, default=p.ROM_NAME, help='ROM Name (e.g. breakout.bin)')
    parser.add_argument('-display_screen', '--display_screen', type=strToBool, default=True, help='Display Screen')
    parser.add_argument('-make_gif', '--make_gif', type=strToBool, default=False, help='Make a Gif')
    parser.add_argument('-make_video', '--make_video', type=strToBool, default=False, help='Make a Video')
    parser.add_argument('-sound', '--sound', type=strToBool, default=p.SOUND, help='Sound')
    parser.add_argument('-color_averaging', '--color_averaging', type=strToBool, default=p.COLOR_AVERAGING, help='Color Averaging')
    parser.add_argument('-frame_skip', '--frame_skip', type=int, default=p.FRAME_SKIP, help='Act Repeat')
    parser.add_argument('-random_starts', '--random_starts', type=int, default=p.RANDOM_STARTS, help='Random Starts')
    parser.add_argument('-minimal_action_set', '--minimal_action_set', type=strToBool, default=p.MINIMAL_ACTION_SET, help='Minimal Action Set')
    parser.add_argument('-repeat_action_prob', '--repeat_action_prob', type=float, default=p.REPEAT_ACTION_PROB, help='Repeat Action Probability')
    parser.add_argument('-epsilon_start', '--epsilon_start', type=float, default=p.EPSILON_START, help='Epsilon Start')
    parser.add_argument('-epsilon_end', '--epsilon_end', type=float, default=p.EPSILON_END, help='Epsilon End')
    parser.add_argument('-epsilon_end_time', '--epsilon_end_time', type=int, default=p.EPSILON_END_TIME, help='Epsilon End Time')
    parser.add_argument('-testing_epsilon', '--testing_epsilon', type=float, default=p.TESTING_EPSILON, help='Testing Epsilon')
    parser.add_argument('-learning_rate', '--learning_rate', type=float, default=p.LEARNING_RATE, help='Learning Rate')
    parser.add_argument('-rmsprop_rho', '--rmsprop_rho', type=float, default=p.RMSPROP_RHO, help='RMSprop rho')
    parser.add_argument('-rmsprop_epsilon', '--rmsprop_epsilon', type=float, default=p.RMSPROP_EPSILON, help='RMSprop epsilon')
    parser.add_argument('-batch_size', '--batch_size', type=int, default=p.BATCH_SIZE, help='Batch Size')
    parser.add_argument('-target_net_update', '--target_net_update', type=int, default=p.TARGET_NET_UPDATE, help='Target Net Update')
    parser.add_argument('-max_reward', '--max_reward', type=int, default=p.MAX_REWARD, help='Max Reward')
    parser.add_argument('-min_reward', '--min_reward', type=int, default=p.MIN_REWARD, help='Min Reward')
    parser.add_argument('-discount_factor', '--discount_factor', type=float, default=p.DISCOUNT_FACTOR, help='Discount Factor (gamma)')
    parser.add_argument('-update_frequency', '--update_frequency', type=int, default=p.UPDATE_FREQUENCY, help='Update Frequency')
    parser.add_argument('-learn_start', '--learn_start', type=int, default=p.LEARN_START, help='Learn Start')
    parser.add_argument('-resize_width', '--resize_width', type=int, default=p.RESIZE_WIDTH, help='Resize Width')
    parser.add_argument('-resize_height', '--resize_height', type=int, default=p.RESIZE_HEIGHT, help='Resize Height')
    parser.add_argument('-replay_memory_size', '--replay_memory_size', type=int, default=p.REPLAY_MEMORY_SIZE, help='Replay Memory Size')
    parser.add_argument('-agent_history_length', '--agent_history_length', type=int, default=p.AGENT_HISTORY_LENGTH, help='Agent History Length')
    args = parser.parse_args()

    seed = np.random.randint(1000)
    rng = np.random.RandomState(seed=seed)
    print "Seed:", seed
    firstRandInt = rng.randint(1, (2 ** 32) + 1)
    print "Net Seed:", firstRandInt
    set_rng(np.random.RandomState(seed=firstRandInt))

    environment_settings =  {
        'DISPLAY_SCREEN': args.display_screen,
        'SOUND':args.sound,
        'COLOR_AVERAGING': args.color_averaging,
        'RANDOM_SEED': seed,
        'RNG': rng,
        'FRAME_SKIP': args.frame_skip,
        'ROMS_DIR':args.roms_dir,
        'ROM_NAME':args.rom_name,
        'RANDOM_STARTS':args.random_starts,
        'MINIMAL_ACTION_SET':args.minimal_action_set,
        'REPEAT_ACTION_PROB':args.repeat_action_prob
    }

    agent_settings = {
        'RNG': rng,
        'EPSILON_START':args.epsilon_start,
        'EPSILON_END':args.epsilon_end,
        'EPSILON_END_TIME': args.epsilon_end_time,
        'TESTING_EPSILON': args.testing_epsilon,
        'LEARNING_RATE':args.learning_rate,
        'RMSPROP_RHO':args.rmsprop_rho,
        'RMSPROP_EPSILON': args.rmsprop_epsilon,
        'BATCH_SIZE':args.batch_size,
        'TARGET_NET_UPDATE':args.target_net_update,
        'MAX_REWARD':args.max_reward,
        'MIN_REWARD':args.min_reward,
        'DISCOUNT_FACTOR':args.discount_factor,
        'UPDATE_FREQUENCY':args.update_frequency,
        'LEARN_START':args.learn_start,
        'RESIZE_WIDTH':args.resize_width,
        'RESIZE_HEIGHT':args.resize_height,
        'REPLAY_MEMORY_SIZE':args.replay_memory_size,
        'AGENT_HISTORY_LENGTH':args.agent_history_length
    }

    main(environment_settings, agent_settings, args)
