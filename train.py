#!/usr/bin/env python
# coding=utf-8

import os
import time
import gzip
import cPickle
import logging
import argparse
import numpy as np

from dqn.dnn import set_rng
from hyperparameters import Hyperparameters
from dqn.environment import GameEnvironment
from dqn.agent import NeuralQLearner

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s : %(message)s'
)

logger = logging.getLogger('Training-Agent')

def main(environment_settings, agent_settings, args):

    game_env = GameEnvironment(environment_settings)
    rom_name = game_env.name
    agent_settings['N_ACTIONS'] = game_env.n_actions
    agent = NeuralQLearner(agent_settings)
    learn_start = agent.learn_start
    frame_skip = environment_settings['FRAME_SKIP']
    reward_history = [ ]
    episode_history = [ ]
    step = 0
    max_step = args.epochs * args.steps_per_epoch
    logger.info("Max Step -> %d", max_step)
    screen, reward, terminal, game_over = game_env.getState()
    logger.info("Start Training...")
    start_time = time.time()
    while(step < max_step):

        step += 1
        action_index = agent.perceive(screen, reward, terminal, False)

        if(not game_over):
            screen, reward, terminal, game_over = game_env.step(action_index, training=True)
        else:
            screen, reward, terminal, game_over = game_env.newGame()

        if((step % args.steps_per_epoch == 0) and (step > learn_start)):

            training_time = time.time() - start_time
            epoch = step / args.steps_per_epoch
            logger.info("No. Epochs -> %d", epoch)

            screen, reward, terminal, game_over = game_env.newTestGame()

            total_reward = 0
            nrewards = 0
            nepisodes = 0
            max_reward = 0
            episode_reward = 0

            logger.info("Start Testing...")
            eval_time = time.time()
            for i in xrange(args.steps_per_test):

                action = agent.perceive(screen, reward, terminal, True)

                screen, reward, terminal, game_over = game_env.step(action, training=False)

                episode_reward += reward

                if(not(reward == 0)):
                    nrewards += 1

                if(terminal):
                    total_reward += episode_reward
                    if(episode_reward > max_reward):
                        max_reward = episode_reward
                    episode_reward = 0
                    nepisodes += 1
                    screen, reward, terminal, game_over = game_env.newGame()

            eval_time = time.time() - eval_time
            logger.info("Finish Testing...")
            episode_history.append(nepisodes)
            total_reward = float(total_reward)/float(max(1, nepisodes))

            f = gzip.open('snapshots/'+rom_name+'_weights_epoch_'+str(epoch)+'.pkl.gz', 'wb')
            cPickle.dump(agent.net.getWeights(), f, protocol=cPickle.HIGHEST_PROTOCOL)
            f.close()

            reward_history.append(total_reward)

            training_rate = float(frame_skip*(args.steps_per_epoch))/float(training_time)

            logger.info('Steps: %d (frames: %d)', step, step * frame_skip)
            logger.info('Epside Max Reward -> %d', max_reward)
            logger.info('Average Reward: %.2f', total_reward)
            logger.info('Last Training Epsilon: %.2f', agent.getEpsilon())
            logger.info('Training Time: %ds', training_time)
            logger.info('Training Rate: %dfps', training_rate)
            logger.info('Testing Time: %ds', eval_time)
            logger.info('Testing Rate: %dfps', (float(frame_skip * args.steps_per_test)/float(eval_time)))
            logger.info('Num. Ep.: %d', nepisodes)
            logger.info('Num. Rewards: %d', nrewards)

            os.system('echo "Steps: %d (frames: %d)" >> results/%s_training.log' % (step, step * frame_skip, rom_name))
            os.system('echo "Epside Max Reward -> %d" >> results/%s_training.log' % (max_reward, rom_name))
            os.system('echo "Average Reward: %.2f" >> results/%s_training.log' % (total_reward, rom_name))
            os.system('echo "Last Training Epsilon: %.2f" >> results/%s_training.log' % (agent.getEpsilon(), rom_name))
            os.system('echo "Training Time: %ds" >> results/%s_training.log' % (training_time, rom_name))
            os.system('echo "Training Rate: %dfps" >> results/%s_training.log' % (training_rate, rom_name))
            os.system('echo "Testing Time: %ds" >> results/%s_training.log' % (eval_time, rom_name))
            os.system('echo "Testing Rate: %dfps" >> results/%s_training.log' % ((float(frame_skip * args.steps_per_test)/float(eval_time)), rom_name))
            os.system('echo "Num. Ep.: %d" >> results/%s_training.log' % (nepisodes, rom_name))
            os.system('echo "Num. Rewards: %d" >> results/%s_training.log' % (nrewards, rom_name))
            os.system('echo "---------------------------------------" >> results/%s_training.log' % (rom_name))

            f = gzip.open('results/'+rom_name+'_reward_history.pkl.gz', 'wb')
            cPickle.dump(reward_history, f, protocol=cPickle.HIGHEST_PROTOCOL)
            f.close()
            f = gzip.open('results/'+rom_name+'_episode_history.pkl.gz', 'wb')
            cPickle.dump(episode_history, f, protocol=cPickle.HIGHEST_PROTOCOL)
            f.close()

            start_time = time.time()

        if(step % 10000 == 0):
            logger.info("Steps -> %d, Frames -> %d, Steps Left -> %d", step, step * frame_skip, max_step-step)

    logger.info("All Done")

if __name__ == '__main__':

    def strToBool(str):
        if(str.upper() in ['T','TRUE','1']):
            return True
        else:
            return False

    p = Hyperparameters()
    parser = argparse.ArgumentParser('Training Agent')
    parser.add_argument('-epochs', '--epochs', type=int, default=p.EPOCHS, help='Epochs')
    parser.add_argument('-steps_per_epoch', '--steps_per_epoch', type=int, default=p.STEPS_PER_EPOCH, help='Steps x Epoch')
    parser.add_argument('-steps_per_test', '--steps_per_test', type=int, default=p.STEPS_PER_TEST, help='Steps x Test')
    parser.add_argument('-roms_dir', '--roms_dir', type=str, default=p.ROMS_DIR, help='ROMS Directory')
    parser.add_argument('-rom_name', '--rom_name', type=str, default=p.ROM_NAME, help='ROM Name (e.g. breakout.bin)')
    parser.add_argument('-display_screen', '--display_screen', type=strToBool, default=p.DISPLAY_SCREEN, help='Display Screen')
    parser.add_argument('-sound', '--sound', type=strToBool, default=p.SOUND, help='Sound')
    parser.add_argument('-color_averaging', '--color_averaging', type=strToBool, default=p.COLOR_AVERAGING, help='Color Averaging')
    parser.add_argument('-random_seed', '--random_seed', type=int, default=p.RANDOM_SEED, help='Random Seed')
    parser.add_argument('-frame_skip', '--frame_skip', type=int, default=p.FRAME_SKIP, help='Act Repeat')
    parser.add_argument('-random_starts', '--random_starts', type=int, default=p.RANDOM_STARTS, help='Random Starts')
    parser.add_argument('-minimal_action_set', '--minimal_action_set', type=strToBool, default=p.MINIMAL_ACTION_SET, help='Minimal Action Set')
    parser.add_argument('-repeat_action_prob', '--repeat_action_prob', type=float, default=p.REPEAT_ACTION_PROB, help='Repeat Action Probability')
    parser.add_argument('-epsilon_start', '--epsilon_start', type=float, default=p.EPSILON_START, help='Epsilon Start')
    parser.add_argument('-epsilon_end', '--epsilon_end', type=float, default=p.EPSILON_END, help='Epsilon End')
    parser.add_argument('-epsilon_end_time', '--epsilon_end_time', type=int, default=p.EPSILON_END_TIME, help='Epsilon End Time')
    parser.add_argument('-testing_epsilon', '--testing_epsilon', type=float, default=p.TESTING_EPSILON, help='Testing Epsilon')
    parser.add_argument('-learning_rate', '--learning_rate', type=float, default=p.LEARNING_RATE, help='Learning Rate')
    parser.add_argument('-rmsprop_rho', '--rmsprop_rho', type=float, default=p.RMSPROP_RHO, help='RMSprop rho')
    parser.add_argument('-rmsprop_epsilon', '--rmsprop_epsilon', type=float, default=p.RMSPROP_EPSILON, help='RMSprop epsilon')
    parser.add_argument('-batch_size', '--batch_size', type=int, default=p.BATCH_SIZE, help='Batch Size')
    parser.add_argument('-target_net_update', '--target_net_update', type=int, default=p.TARGET_NET_UPDATE, help='Target Net Update')
    parser.add_argument('-max_reward', '--max_reward', type=int, default=p.MAX_REWARD, help='Max Reward')
    parser.add_argument('-min_reward', '--min_reward', type=int, default=p.MIN_REWARD, help='Min Reward')
    parser.add_argument('-discount_factor', '--discount_factor', type=float, default=p.DISCOUNT_FACTOR, help='Discount Factor (gamma)')
    parser.add_argument('-update_frequency', '--update_frequency', type=int, default=p.UPDATE_FREQUENCY, help='Update Frequency')
    parser.add_argument('-learn_start', '--learn_start', type=int, default=p.LEARN_START, help='Learn Start')
    parser.add_argument('-resize_width', '--resize_width', type=int, default=p.RESIZE_WIDTH, help='Resize Width')
    parser.add_argument('-resize_height', '--resize_height', type=int, default=p.RESIZE_HEIGHT, help='Resize Height')
    parser.add_argument('-replay_memory_size', '--replay_memory_size', type=int, default=p.REPLAY_MEMORY_SIZE, help='Replay Memory Size')
    parser.add_argument('-agent_history_length', '--agent_history_length', type=int, default=p.AGENT_HISTORY_LENGTH, help='Agent History Length')
    args = parser.parse_args()

    rng = np.random.RandomState(seed=args.random_seed)
    print "Seed:", args.random_seed
    firstRandInt = rng.randint(1, (2 ** 32) + 1)
    print "Net Seed:", firstRandInt
    set_rng(np.random.RandomState(seed=firstRandInt))

    environment_settings =  {
        'DISPLAY_SCREEN': args.display_screen,
        'SOUND':args.sound,
        'COLOR_AVERAGING': args.color_averaging,
        'RANDOM_SEED': args.random_seed,
        'RNG': rng,
        'FRAME_SKIP': args.frame_skip,
        'ROMS_DIR':args.roms_dir,
        'ROM_NAME':args.rom_name,
        'RANDOM_STARTS':args.random_starts,
        'MINIMAL_ACTION_SET':args.minimal_action_set,
        'REPEAT_ACTION_PROB':args.repeat_action_prob
    }

    agent_settings = {
        'RNG': rng,
        'EPSILON_START':args.epsilon_start,
        'EPSILON_END':args.epsilon_end,
        'EPSILON_END_TIME': args.epsilon_end_time,
        'TESTING_EPSILON': args.testing_epsilon,
        'LEARNING_RATE':args.learning_rate,
        'RMSPROP_RHO':args.rmsprop_rho,
        'RMSPROP_EPSILON': args.rmsprop_epsilon,
        'BATCH_SIZE':args.batch_size,
        'TARGET_NET_UPDATE':args.target_net_update,
        'MAX_REWARD':args.max_reward,
        'MIN_REWARD':args.min_reward,
        'DISCOUNT_FACTOR':args.discount_factor,
        'UPDATE_FREQUENCY':args.update_frequency,
        'LEARN_START':args.learn_start,
        'RESIZE_WIDTH':args.resize_width,
        'RESIZE_HEIGHT':args.resize_height,
        'REPLAY_MEMORY_SIZE':args.replay_memory_size,
        'AGENT_HISTORY_LENGTH':args.agent_history_length
    }

    main(environment_settings, agent_settings, args)
